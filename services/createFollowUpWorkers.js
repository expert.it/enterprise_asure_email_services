const os = require('os');
const path = require('path');
const fetch = (...args) =>
  import('node-fetch').then(({ default: fetch }) => fetch(...args));
const { Worker, workerData } = require('node:worker_threads');
const Campaign_Model = require('../models/campaign');
const { Mutex, acquireMutex } = require('../utils/MutexLock');
const Queue = require('../utils/Queue');
const mailboxesApi = require('./mailBoxes');

const { getHrsInMS, getMinsInMS } = require('../utils/customTime');
const { writeJSON } = require('../utils/ReadWriteJSON');
const {
  loggerAdv,
  followUpConfig: {
    workerPerCore,
    POLL_EVERY_X_MINS,
    RETRY_ON_ERROR_AFTER_X_MINS,
    RETRY_FOR_RESOURCES_IN_X_MINS,
  },
  azureEmailService: { RESOURCE_COOLING_TIME_IN_HRS },
} = require('../config/config');
const { addToQueue, assignMailboxesIfReq } = require('./createEmailWorkers');

const prefix = '[Follow-Up]';
const print = (str) => {
  console.log(`${prefix} ${str}`);
};
const logger = loggerAdv(prefix);

// const resConfFilePath = path.join(
//   __dirname,
//   '..',
//   'config/azureResourcesConfig.json',
// );

const CORES = os.cpus().length;
// multiply by x, for x processes per CORE [not strict (managed by OS's context switching mech!)]
const MAX_WORKERS = (CORES - 1) * workerPerCore;
// to prevent race conditions
// **IMP: make sure to aquire lock before updating this(AVAILABLE_WORKERS) variable!
let AVAILABLE_WORKERS = MAX_WORKERS;
const workerMutex = new Mutex();

// const queue = new Queue();
const set = new Set();

const synchroniseSet = (id) => {
  return set.delete(id);
};

const inProcess = new Set();
const getServiceState = () => {
  const campaignIds = Array.from(inProcess.values());
  const state = {
    // workers: {
    //   free: AVAILABLE_WORKERS,
    //   total: MAX_WORKERS,
    //   threadingFactor: workerPerCore,
    // },
    // nextWorker: {
    //   id: queue.peek() ? queue.headIndex : null,
    //   campaignId: queue.peek(),
    // },
    // inProcess: {
    //   activeWorkerCount: inProcess.size,
    //   campaignIds_size: campaignIds.length,
    //   campaignIds,
    // },
    // queueLength: Object.keys(queue.items).length,
    // queue,
    setLength: set.size,
    // set: Array.from(set),
    // mutex: workerMutex.getState(),
  };
  return state;
};

// const spawnWorker = async () => {
//   await workerMutex.acquire();
//   if (AVAILABLE_WORKERS > 0) {
//     if (queue.isEmpty()) {
//       logger.warning(`Worker spawn Requested for empty Q, Rejecting ...`);
//       return;
//     }
//     const [status, index, resource] = await allocateAzureResource({
//       campaignId: queue.peek(),
//     });
//     if (!status) {
//       logger.warning(
//         `Resources not available, Retrying after ${RETRY_FOR_RESOURCES_IN_X_MINS} mins`,
//       );
//       setTimeout(spawnWorker, getMinsInMS(RETRY_FOR_RESOURCES_IN_X_MINS));
//       return;
//     }

//     AVAILABLE_WORKERS -= 1;
//     const id = queue.dequeue();
//     const worker = new Worker('./services/followUpService_NEW', {
//       workerData: { campaignId: id, emailResource: resource },
//     });
//     inProcess.add(id);
//     print(`[${worker.threadId}] Worker initiated: [${id}]`);
//     worker.on('message', async ({ status, error, data }) => {
//       // handle job completion
//       if (status === 1) {
//         print(`Process [${worker.threadId}] completed`);

//         // const resConfig = require(resConfFilePath);
//         // resConfig.resourcePool[index] = data;
//         // writeJSON(resConfig, resConfFilePath);
//         mailboxesApi
//           .updateMailBoxes({
//             filter: {
//               _id: resource._id,
//             },
//             update: {
//               $set: data,
//             },
//           })
//           .then((res) => {
//             print(`[${resource.senderEmail}] Resource updated!`);
//           });

//         set.delete(id);
//       }
//       // handle job failure
//       else if (status === 0) {
//         print(
//           `Process [${worker.threadId}] failed: error-${error}`,
//         );

//         // const resConfig = require(resConfFilePath);
//         // resConfig.resourcePool[index] = data;
//         // writeJSON(resConfig, resConfFilePath);
//         mailboxesApi
//           .updateMailBoxes({
//             filter: {
//               _id: resource._id,
//             },
//             update: {
//               $set: data,
//             },
//           })
//           .then((res) => {
//             print(`[${resource.senderEmail}] Resource updated!`);
//           });

//         print(
//           `[${worker.threadId}] Retrying in ${RETRY_ON_ERROR_AFTER_X_MINS} min`,
//         );
//         setTimeout(() => {
//           queue.enqueue(id);
//           spawnWorker();
//         }, getMinsInMS(RETRY_ON_ERROR_AFTER_X_MINS)); // start workers after x min
//       }
//       await worker.terminate();
//     });

//     worker.on('error', (error) => print(error));

//     worker.on('exit', async (code) => {
//       // print(
//       //   `[${worker.threadId}] A process terminated with exit code - ${code}`,
//       // );
//       inProcess.delete(id);
//       await workerMutex.acquire({ priority: true });
//       AVAILABLE_WORKERS += 1;
//       workerMutex.release();
//       // logger.warning(`Workers, available: ${AVAILABLE_WORKERS}/${MAX_WORKERS}`);
//       if (!queue.isEmpty()) {
//         spawnWorker();
//       }
//     });
//   } else {
//     logger.error(
//       `Workers busy, available: ${AVAILABLE_WORKERS}/${MAX_WORKERS}`,
//     );
//   }
//   workerMutex.release();
// };

const handleEnqueue = async (id, by) => {
  // logger.success('Set items:')
  // set.forEach((key) => logger.warning(key));
  // logger.error(`Set has ${id}: ${set.has(id.toString())}`);

  if (set.has(id)) return;
  set.add(id);
  // queue.enqueue(id);
  const enqueuStatus = await addToQueue({
    _id: id,
    followUp: true,
    myRefs: { synchroniseSet },
  });
  logger.focus(
    `Enqueue rquested for campaign (${id}) by '${by}': ${enqueuStatus}`,
  );
  // await spawnWorker();
};

const startProcessing = async (data) => {
  let { _id: id, clientEmail: email } = data;
  const _id = id.toString();
  if (
    set.has(_id) ||
    // allow list
    ![
      // ivan_omelchenko@wow24-7.io
      '64f9b0984fb0ab00573ca88a',
      // sdecosmo@amplifyhrm.com
      // '65008938e18c4f0056341b57',
      // dio@leadmark
      // '64bd9075c29f0400501e646f',
      //
      // '64f9bb534fb0ab00573cb286',
      // '64e7171fe1b5eb46d438898e',
      // '649ef9b3dce08300502bc432',
    ].includes(_id)
  ) {
    print(`Discarded: ${_id}`);
    return;
  }
  await assignMailboxesIfReq({ _id, email });
  await handleEnqueue(_id, 'Polling Service');
};

const pollDataForProcessing = async () => {
  const now = new Date().toUTCString();
  print(`Polling started @${now.split(',')[1]}`);
  try {
    const data = await Campaign_Model.find({}, { _id: 1, clientEmail: 1 });
    // print(data);

    if (!data.length) {
      logger.focus(`No campaigns found by polling service`);
      return;
    }

    data.forEach(async (campaign) => {
      await startProcessing(campaign._doc);
    });
  } catch (e) {
    print(e);
    print(`Polling failed with error: `, e.message);
  }
};

const startPolling = () => {
  logger.warning(`Polling happens every ${POLL_EVERY_X_MINS} mins`);
  pollDataForProcessing();
  const pollingInt = setInterval(
    pollDataForProcessing,
    getMinsInMS(POLL_EVERY_X_MINS),
  );
  return () => clearInterval(pollingInt);
};

module.exports = {
  startPolling,
  getServiceState,
};
