const { EmailClient } = require('@azure/communication-email');
const { getHrsInMS, getMinsInMS } = require('../utils/customTime');
const { writeJSON } = require('../utils/ReadWriteJSON');
const { timeOut, random } = require('../utils/misc');
const {
  workerConfig: {
    maxEmailPerRequest,
    maxEmailsPerDomain,
    MIN_LEADS,
    TIMEOUT_EMAIL_SENDING_IN_SECS,
    TIMEOUT_AI_CONTENT_GEN_IN_SECS,
    POLL_EVERY_X_MINS,
    RETRY_ON_ERROR_AFTER_X_MINS,
    RETRY_FOR_RESOURCES_IN_X_MINS,
  },
  logger,
  azureResourcePath: resConfFilePath,
  azureEmailService: { LIMIT_PER_HOUR, RESOURCE_COOLING_TIME_IN_HRS },
  OPEN_AI,
  API_SERVICE_BACKEND,
} = require('../config/config');
const DailyUpdate_Model = require('../models/dailyUpdate');
const Queue = require('../utils/Queue');

const queue = new Queue();
const set = new Set();

const addToQueue = (obj) => {
  queue.enqueue(obj);
};

const freeResources = () => {
  const resConfig = require(resConfFilePath);
  for (let i = 0; i < resConfig.resourcePool.length; i += 1) {
    resConfig.resourcePool[i].free = true;
  }
  for (let i = 0; i < resConfig.updatePool.length; i += 1) {
    resConfig.updatePool[i].free = true;
  }
  writeJSON(resConfig, resConfFilePath);
};
freeResources();

const allocateAzureResource = (pool) => {
  // const resConfig = require(resConfFilePath);
  // const { LIMIT_PER_MINUTE, LIMIT_PER_HOUR, [pool]: resources } = resConfig;

  // for (let i = 0; i < resources.length; i += 1) {
  //   const resource = resources[i];
  //   const { free, cooling, lastActive } = resource;

  //   if (!free) continue;
  //   if (cooling) {
  //     if (lastActive + getHrsInMS(RESOURCE_COOLING_TIME_IN_HRS) > Date.now())
  //       continue;
  //     resource.emailsSent = 0;
  //     resource.cooling = false;
  //   }

  //   resource.free = false;
  //   writeJSON(resConfig, resConfFilePath);
  //   return [true, i, resource];
  // }
  // return [false, null, null];

  const resConfig = require(resConfFilePath);
  const { [pool]: resources } = resConfig;

  const availableRes = [];

  for (let i = 0; i < resources.length; i += 1) {
    const resource = resources[i];
    const { free, cooling, lastActive, lastReset } = resource;

    const now = Date.now();
    const nextReset = lastReset + getHrsInMS(RESOURCE_COOLING_TIME_IN_HRS);
    const diff = Math.max(0, nextReset - now);
    if (diff <= 0) {
      resource.emailsSent = 0;
      resource.lastReset = Date.now();
    }

    if (!free) continue;
    if (cooling) {
      // if (lastActive + getHrsInMS(RESOURCE_COOLING_TIME_IN_HRS) > Date.now())
      //   continue;
      if (diff > 0) continue;
      resource.cooling = false;
    }

    // const now = Date.now();
    // while (emailsSent.length && emailsSent[0] + getHrsInMS(RESOURCE_COOLING_TIME_IN_HRS) <= now) {
    //   emailsSent.shift();
    // }

    // if (emailsSent.length < LIMIT_PER_HOUR) {
    //   resource.free = false;
    //   writeJSON(resConfig, resConfFilePath);
    //   return [true, i, resource];
    // }

    availableRes.push([i, resource]);
  }

  if (!Boolean(availableRes.length)) return [false, null, null];

  const [index, selectedRes] = availableRes[random(availableRes.length)];
  // console.log(availableRes);
  selectedRes.free = false;
  // console.log(selectedRes);
  writeJSON(resConfig, resConfFilePath);
  return [true, index, selectedRes];
};

const sendEmailsGlobal = async ({ subject, email, recipient }) => {
  let status, index, resource;
  try {
    [status, index, resource] = allocateAzureResource('updatePool');
    if (!status) {
      logger.warning(
        `Resources not available, Retrying after ${RETRY_FOR_RESOURCES_IN_X_MINS} mins`,
      );
      setTimeout(
        () => sendEmailsGlobal({ email, recipient }),
        getMinsInMS(RETRY_FOR_RESOURCES_IN_X_MINS),
      );
      return;
    }
  } catch (err) {
    console.log(err);
    logger.error(`Resource allocation failed!`);
    return;
  }
  const { connStr, senderEmail } = resource;
  const emailClient = new EmailClient(connStr);
  const message = {
    senderAddress: senderEmail,
    content: {
      subject,
      plainText: email,
      html: `<pre>${email}</pre>`,
    },
    recipients: {
      to: [recipient],
    },
    // attachments: [
    //   {
    //     name: path.basename(filePath),
    //     contentType: "text/plain",
    //     contentInBase64: readFileSync(filePath, "base64"),
    //   },
    // ],
  };

  let waitInt;
  try {
    // logger.warning(
    //   `Email sending process begins for ${recipient.displayName} (${recipient.address})`,
    // );
    waitInt = setInterval(() => {
      logger.warning(
        `[Update] Waiting for process completion ${recipient.displayName} (${recipient.address})`,
      );
    }, 10 * 1000);

    logger.focus(`[Update] Sending Email ...`);
    const poller = await timeOut(
      async () => await emailClient.beginSend(message),
      TIMEOUT_EMAIL_SENDING_IN_SECS * 1000,
      { errorMessageOnTimeout: 'Switch' },
    );
    const response = await poller.pollUntilDone();
    // console.log(response)
    // logger.warning(`Email sent to ${recipient.address}`);
    const { id, status, error } = response;
    // console.log(id)
    const sentEmailObj = {
      from: message.senderAddress,
      to: recipient.address,
      email: {
        subject: message.content.subject,
        text: message.content.plainText,
        html: message.content.html,
      },
    };
    const newSentEmail = new DailyUpdate_Model(sentEmailObj);
    await newSentEmail.save().then((res) => {
      logger.warning(`[Update] Email saved to DB...`);
    });
    return [status, email, error];
  } catch (e) {
    // console.log(e);
    logger.warning(`[Update] sendEmails() catch:`);
    logger.error(e.message);
    // const deniedByProviderPattern = /Denied/
    if (e.message === 'Switch' || e.code === 'Denied') {
      return ['Switch', null, null];
    }

    const invalidEmail = new RegExp(/Invalid format for email address/, 'i');
    if (invalidEmail.test(e.message)) {
      return [
        'Blocked',
        "<b><span style='color:red;'>Invalid Email</span></b>",
        null,
      ];
    }

    const pattern = new RegExp(/EmailDroppedAllRecipientsSuppressed/, 'i');
    if (pattern.test(e.message)) {
      return [
        'Blocked',
        "<b><span style='color:red;'>Permission denied</span><br/>User may have opted out !</b>",
        null,
      ];
    } else {
      throw new Error(e.message);
    }
  } finally {
    const resConfig = require(resConfFilePath);
    resource.free = true;
    resConfig['updatePool'][index] = resource;
    writeJSON(resConfig, resConfFilePath);
    clearInterval(waitInt);
  }
};

const startEmailSending = async () => {
  const obj = queue.peek();
  const [status, email, error] = await sendEmailsGlobal(obj);
  if (status === 'Succeeded' || status === 'Blocked') {
    logger.success(`Email sent to ${obj.recipient.address}`);
    queue.dequeue();
  }
  startEmailSending();
};

module.exports = {
  sendEmailsGlobal,
  startEmailSending,
};
