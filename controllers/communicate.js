const { logger } = require('../config/config');
const { pollDataForProcessing } = require('../services/createEmailWorkers');
const { generateAIEmail } = require('../services/emailService');
const { sendEmailsGlobal } = require('../services/emails');

// accept requests only every 5 secs
let lastRun = 0;
const processOnCampUnpause = async (req, res) => {
  const now = Date.now();
  if (lastRun + 5 * 1000 > now) {
    res.status(204).end();
    return;
  }
  lastRun = now;
  logger.warning(`Triggered polling`);
  pollDataForProcessing();
  res.status(200).end();
};

const sendEmails = (req, res) => {
  const { payload } = req.body;
  // console.log(payload);
  // const testRecipient = {
  //   address: 'gautamsir076@gmail.com',
  //   displayName: 'Gautam Chaurasia',
  // };
  const emailStatus = payload.map(({ subject, email, recipient }) => {
    return sendEmailsGlobal({ subject, email, recipient });
  });
  let count = 0;
  Promise.all(emailStatus).then((statusArr) => {
    // console.log(statusArr);
    if (!Boolean(statusArr.length)) return;
    statusArr.forEach((r) => {
      if (r?.[0] === 'Succeeded') count += 1;
    });
    res.status(200).json({
      status: count,
      message: count
        ? 'Email sent'
        : `Error encountered. We'll try to send email again after some time, don't worry!`,
    });
  });
};

const getSampleEmail = async (req, res) => {
  const { campaign, leads } = req.body;
  // console.log(campaign);
  // console.log(leads);
  try {
    const aiGenPromises = leads.map((lead) => generateAIEmail(campaign, lead));
    const aiGenArr = await Promise.all(aiGenPromises);
    const retData = {};
    leads.forEach((lead, index) => {
      retData[lead.email] = aiGenArr[index];
    });
    // console.log(retData);
    res.status(200).json(retData);
  } catch (err) {
    logger.error(err);
    res.status(500).end();
  }
};

module.exports = {
  processOnCampUnpause,
  sendEmails,
  getSampleEmail,
};
