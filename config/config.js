const colors = require('colors');
const path = require('path');

const azureResourcePath_DEV = path.resolve(
  __dirname,
  'azureResourcesConfig_DEV.json',
);
const azureResourcePath_PROD = path.resolve(
  __dirname,
  'azureResourcesConfig_PROD_v3.json',
);

// LOCAL
// const frontEndURL = 'http://localhost:3000';

// PRODUCTION
const frontEndURL = 'https://app.b2brocket.ai';

module.exports = {
  workerConfig: {
    workerPerCore: 30,
    MIN_LEADS: 20000,
    maxEmailPerRequest: 12,
    maxEmailsPerDomain: 20,
    POLL_EVERY_X_MINS: 2,
    RETRY_ON_ERROR_AFTER_X_MINS: 1,
    RETRY_FOR_RESOURCES_IN_X_MINS: 1,
    TIMEOUT_EMAIL_SENDING_IN_SECS: 45,
    TIMEOUT_AI_CONTENT_GEN_IN_SECS: 60,
  },

  followUpConfig: {
    workerPerCore: 2,
    maxEmailPerRequest: 14,
    POLL_EVERY_X_MINS: 24 * 60, // once a day
    RETRY_ON_ERROR_AFTER_X_MINS: 1,
    RETRY_FOR_RESOURCES_IN_X_MINS: 1,
    TIMEOUT_EMAIL_SENDING_IN_SECS: 45,
    // 15 mins in days: 0.01041666666666666666666666666667
    SEND_EMAILS_AFTER_X_DAYS: 2,
    // emailSeqStages: {
    //   initial: {
    //     // in days
    //     step1: 0,
    //     step2: 10,
    //     step3: 22,
    //     step4: 37,
    //     step5: 50,
    //   },
    //   conversation: {},
    //   meeting: {},
    // },
  },

  emailConfig: {
    activeHrs: {
      start: 6,
      end: 21,
      tz: 'America/New_York',
    },
    // 6-21 => 15hr window => (2hr buffer, available time: 13hrs) 13*60 = 780mins/20emails-per-mailbox-per-day = max gap allowed between each email '39mins {-5mins for email generation = 34mins}' => settled for ->30mins)
    maxEmailingGapInMins: 15,
    maxAllowedEmailsPerDay: 20,
    incEmailCapBy: 2, // obsolete
    MAILBOXES_PER_CAMPAIGN: 10,
    googleWebAppConfig: {
      client_id: '77176128363-v1m3kia8q8gng7ikn7fujd9h08903ttf.apps.googleusercontent.com',
      client_secret: process.env.GOOGLE_WEB_APP_CLIENT_SECRET,
      token_url: 'https://oauth2.googleapis.com/token'
    }
  },

  mailBoxesRampUpSchedule: {
    weeks: {
      // week: num of emails
      1: 8,
      2: 10,
      3: 12,
      4: 14,
      5: 16,
      6: 16,
      7: 18,
      8: 18,
      9: 19,
      10: 20,
    },
  },

  azureEmailService: {
    LIMIT_PER_HOUR: 10,
    RESOURCE_COOLING_TIME_IN_HRS: 1,
    RESOURCE_RESET_TIME_IN_HRS: 24, // can't be less than 24
  },

  OPEN_AI: {
    URL: 'https://api.openai.com/v1/chat/completions',
    // URL: 'https://open-ai-test.openai.azure.com/openai/deployments/test/chat/completions?api-version=2023-05-15',
    API_KEY: process.env.OPEN_AI_API_KEY,
  },

  googleWebAppConfig: {
    gmail_api_url: 'https://gmail.googleapis.com/gmail/v1',
    token_url: 'https://oauth2.googleapis.com/token',
    oauth_url: 'https://accounts.google.com/o/oauth2/v2/auth',
    client_id:
      '781502629861-ar0v24p5n61uvjhd6nivfp07td8helm7.apps.googleusercontent.com',
    project_id: 'b2b-rocket',
    auth_uri: 'https://accounts.google.com/o/oauth2/auth',
    token_uri: 'https://oauth2.googleapis.com/token',
    auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
    client_secret: process.env.GOOGLE_WEB_APP_CLIENT_SECRET,
    redirect_uris: [
      'http://localhost:3002/addEmail',
      'https://admin.leadingly.io/addEmail.html',
      'https://admin.b2brocket.ai/addEmail.html',
    ],
    // redirect_uri: "http://localhost:3002/addEmail",
    // redirect_uri: "https://admin.leadingly.io/addEmail.html",
    redirect_uri: 'https://admin.b2brocket.ai/addEmail.html',
    javascript_origins: [
      'https://admin.leadingly.io',
      'https://admin.b2brocket.ai',
    ],
    scopes: [
      'https://mail.google.com/',
      'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email',
    ].join(' '),
  },

  // ---------------------------Switch PRODUCTION / LOCAL------------------------------------------------

  // Local
  // frontEndURL: 'http://localhost:3000',
  // azureResourcePath: azureResourcePath_DEV,
  // CUSTOM_GPT_URL: 'http://127.0.0.1:5173/chat',
  // API_SERVICE_BACKEND: 'http://localhost:8080/api/v1/main',

  // API_SERVICE_BACKEND:
  //   '/api/v1/main',

  // Production
  frontEndURL,
  azureResourcePath: azureResourcePath_PROD,
  CUSTOM_GPT_URL: `${frontEndURL}/chat`,
  API_SERVICE_BACKEND:
    'https://campaign-app-server-azure-pipeline.azurewebsites.net/api/v1/main', // leadingly_testapp_backend
  API_SERVICE_BACKEND_2: 'https://leadinglyapi.herokuapp.com/api/v1/main', // leadingly_backend

  // // DEPRECATED!
  // CUSTOM_GPT_URL: 'https://react-sales-agent.vercel.app/chat',
  // CUSTOM_GPT_URL: 'https://brandheroes.io/chat',
  // API_SERVICE_BACKEND:
  //   'https://campaign-app-server-aa5e033e3a87.herokuapp.com/api/v1/main',

  // ----------------------------------------------------------------------------------------------------

  EMAIL_TRACKING_SERVICE:
    'https://emailtrackingtest-3c1c39ac5bfe.herokuapp.com/api/v1/main',

  // Azure
  // CUSTOM_GPT_URL: 'https://lively-smoke-0529ef710.3.azurestaticapps.net/chat',

  // Vercel
  // CUSTOM_GPT_URL: 'https://react-sales-agent.vercel.app/chat',

  logger: {
    error: (txt) => console.log(`${txt}`.red.bold),
    warning: (txt) => console.log(`${txt}`.yellow.bold),
    success: (txt) => console.log(`${txt}`.green.bold),
    focus: (txt) => console.log(`${txt}`.blue.bold),
  },
  loggerAdv: (prefix = '', suffix = '') => {
    let modPrefix = prefix;
    let modSuffix = suffix;
    if (prefix) {
      modPrefix += ' ';
    }
    if (suffix) {
      modSuffix = ' ' + modSuffix;
    }
    return {
      error: (txt) => console.log(`${modPrefix + txt + modSuffix}`.red.bold),
      warning: (txt) =>
        console.log(`${modPrefix + txt + modSuffix}`.yellow.bold),
      success: (txt) =>
        console.log(`${modPrefix + txt + modSuffix}`.green.bold),
      focus: (txt) => console.log(`${modPrefix + txt + modSuffix}`.blue.bold),
    };
  },
};
