const { getDaysInMS } = require('../utils/customTime');

const EMAIL_JOURNEY_TIMELINES = {
  // Numbers represent days
  initial: {
    1: { timeToWaitSincePrevContactInMS: getDaysInMS(0) },
    2: { timeToWaitSincePrevContactInMS: getDaysInMS(10) },
    3: { timeToWaitSincePrevContactInMS: getDaysInMS(12) },
    4: { timeToWaitSincePrevContactInMS: getDaysInMS(15) },
    5: { timeToWaitSincePrevContactInMS: getDaysInMS(13) },
  },
  conversation: {
    1: { timeToWaitSincePrevContactInMS: getDaysInMS(1) },
    2: { timeToWaitSincePrevContactInMS: getDaysInMS(3) },
    3: { timeToWaitSincePrevContactInMS: getDaysInMS(7) },
    4: { timeToWaitSincePrevContactInMS: getDaysInMS(14) },
    5: { timeToWaitSincePrevContactInMS: getDaysInMS(28) },
    6: { timeToWaitSincePrevContactInMS: getDaysInMS(60) },
    7: { timeToWaitSincePrevContactInMS: getDaysInMS(120) },
    8: { timeToWaitSincePrevContactInMS: getDaysInMS(14) },
  },
  meetingBooked: {},
  missedMeeting: {},
};

const TEMPLATE_VARS = [
  {
    var: '[PROSPECT_FIRST_NAME]',
    pattern: '\\[PROSPECT_FIRST_NAME\\]',
    section: 'pFName',
  },
  {
    var: '[PROSPECT_TITLE]',
    pattern: '\\[PROSPECT_TITLE\\]',
    section: 'pTitle',
  },
  {
    var: '[PROSPECT_COMPANY]',
    pattern: '\\[PROSPECT_COMPANY\\]',
    section: 'pCompany',
  },
  {
    var: '[PROSPECT_COMPANY_MISSION]',
    pattern: '\\[PROSPECT_COMPANY_MISSION\\]',
    section: 'pMission',
  },
  {
    var: '[PROSPECT_COMPANY_SPECIFIC_ACHIEVEMENT]',
    pattern: '\\[PROSPECT_COMPANY_SPECIFIC_ACHIEVEMENT\\]',
    section: 'pAchievements',
  },
  {
    var: '[PROSPECT_COMPANY_EXPERTISE]',
    pattern: '\\[PROSPECT_COMPANY_EXPERTISE\\]',
    section: 'pExpertise',
  },
  {
    var: '[PROSPECT_AREA_OF_IMPROVEMENT]',
    pattern: '\\[PROSPECT_AREA_OF_IMPROVEMENT\\]',
    section: 'pImprovement',
  },
  {
    var: '[CHALLENGE_RELATED_TO_SERVICE/PRODUCT]',
    pattern: '\\[CHALLENGE_RELATED_TO_SERVICE/PRODUCT\\]',
    section: 'challenges',
  },
  {
    var: '[USER_COMPANY_DESCRIPTION]',
    pattern: '\\[USER_COMPANY_DESCRIPTION\\]',
    section: 'description',
  },
  {
    var: '[USER_SERVICE/PRODUCT_DESCRIPTION]',
    pattern: '\\[USER_SERVICE/PRODUCT_DESCRIPTION\\]',
    section: 'productDesc',
  },
  {
    var: '[USER_EXPERTISE]',
    pattern: '\\[USER_EXPERTISE\\]',
    section: 'expertise',
  },
  {
    var: '[USER_COMPANY]',
    pattern: '\\[USER_COMPANY\\]',
    section: 'userOrgName',
  },
  {
    var: '[USER_VALUE_PROPOSITION]',
    pattern: '\\[USER_VALUE_PROPOSITION\\]',
    section: 'valueProposition',
  },
  {
    var: '[TIE_IN_TO_WHY_WE_ARE_REACHING_OUT]',
    pattern: '\\[TIE_IN_TO_WHY_WE_ARE_REACHING_OUT\\]',
    section: 'connection',
  },
  { var: '[USER_SIGNATURE]', pattern: '\\[USER_SIGNATURE\\]', section: 'sign' },
  { var: '[DISCLAIMER]', pattern: '\\[DISCLAIMER\\]', section: 'disclaimer' },
  // New Variables
  { var: '[USER_FIRST_NAME]', pattern: '\\[USER_FIRST_NAME\\]' },
  { var: '[PROSPECT_FULL_NAME]', pattern: '\\[PROSPECT_FULL_NAME\\]' },
  { var: '[PROSPECT_EMAIL]', pattern: '\\[PROSPECT_EMAIL\\]' },
  { var: '[PROSPECT_LOCATION]', pattern: '\\[PROSPECT_LOCATION\\]' },
  { var: '[PROSPECT_LINKEDIN]', pattern: '\\[PROSPECT_LINKEDIN\\]' },
  {
    var: '[HOW_USER_SERVICE_CAN_HELP_PROSPECT_COMPANY]',
    pattern: '\\[HOW_USER_SERVICE_CAN_HELP_PROSPECT_COMPANY\\]',
  },
  {
    var: '[EXTENDED_DESCRIPTION_OF_USER_SERVICE]',
    pattern: '\\[EXTENDED_DESCRIPTION_OF_USER_SERVICE\\]',
  },
  { var: '[CURRENT_MONTH]', pattern: '\\[CURRENT_MONTH\\]' },
  { var: '[NEXT_MONTH]', pattern: '\\[NEXT_MONTH\\]' },
  { var: '[TIMESLOT_#1]', pattern: '\\[TIMESLOT_#1\\]' },
  { var: '[TIMESLOT_#2]', pattern: '\\[TIMESLOT_#2\\]' },
  { var: '[TIMESLOT_#3]', pattern: '\\[TIMESLOT_#3\\]' },
  {
    var: '[USER_KEY_SELLING_POINT_#1]',
    pattern: '\\[USER_KEY_SELLING_POINT_#1\\]',
  },
  {
    var: '[USER_KEY_SELLING_POINT_#2]',
    pattern: '\\[USER_KEY_SELLING_POINT_#2\\]',
  },
  {
    var: '[USER_KEY_SELLING_POINT_#3]',
    pattern: '\\[USER_KEY_SELLING_POINT_#3\\]',
  },
  { var: '[MEETING_DATE_AND_TIME]', pattern: '\\[MEETING_DATE_AND_TIME\\]' },
  { var: '[MEETING_TIMEZONE]', pattern: '\\[MEETING_TIMEZONE\\]' },
  { var: '[USER_VALUE_PROP_#1]', pattern: '\\[USER_VALUE_PROP_#1\\]' },
  { var: '[USER_VALUE_PROP_#2]', pattern: '\\[USER_VALUE_PROP_#2\\]' },
  { var: '[USER_VALUE_PROP_#3]', pattern: '\\[USER_VALUE_PROP_#3\\]' },
  { var: '[PROSPECT_TIMEZONE]', pattern: '\\[PROSPECT_TIMEZONE\\]' },
  { var: '[USER_WEBSITE]', pattern: '\\[USER_WEBSITE\\]' },
];

const TEMPLATES = {
  initial: {
    // client
    user: [],
    // lead
    prospect: [
      {
        body: `<p>Hi [PROSPECT_FIRST_NAME],</p><p></p><p>It's hard not to notice <b>[PROSPECT_COMPANY]</b>'s impressive dedication to [PROSPECT_COMPANY_MISSION]. It's truly inspiring.</p><p>At <b>[USER_COMPANY]</b>, we specialize in [USER_SERVICE/PRODUCT_DESCRIPTION], and we believe that our offerings could significantly contribute to [PROSPECT_COMPANY] by enhancing your [PROSPECT_AREA_OF_IMPROVEMENT].</p><p>Would you be available for a discussion to explore how [USER_COMPANY] can support your objectives?</p><p></p>[USER_SIGNATURE][DISCLAIMER]`,
        variables: [
          '[PROSPECT_FIRST_NAME]',
          '[PROSPECT_COMPANY]',
          '[PROSPECT_COMPANY_MISSION]',
          '[PROSPECT_AREA_OF_IMPROVEMENT]',
          '[USER_SERVICE/PRODUCT_DESCRIPTION]',
          '[USER_COMPANY]',
          '[USER_SIGNATURE]',
          '[DISCLAIMER]',
        ],
      },
    ],
  },
  conversation: {
    user: [
      {
        body: `<p>Hi [USER_FIRST_NAME],</p><p></p><p>[PROSPECT_FULL_NAME] has interacted with your AI agent, and we are following up with them to book a meeting with you.</p><p></p><p>If you still would like to follow up with the prospect separately, you can find the transcript and also follow up from the Full Conversation.</p><p></p><p>Email: [PROSPECT_EMAIL]</p><p>Title: [PROSPECT_TITLE]</p><p>Location: [PROSPECT_LOCATION]</p><p>LinkedIn: [PROSPECT_LINKEDIN]</p><p>Company: [PROSPECT_COMPANY]</p><p></p><p>You can also track your conversations from the dashboard.</p><p></p><p>Enjoy the rest of your week!</p><p></p>[USER_SIGNATURE][DISCLAIMER]`,
        variables: [
          '[USER_FIRST_NAME]',
          '[PROSPECT_FULL_NAME]',
          '[PROSPECT_EMAIL]',
          '[PROSPECT_TITLE]',
          '[PROSPECT_LOCATION]',
          '[PROSPECT_LINKEDIN]',
          '[PROSPECT_COMPANY]',
          '[USER_SIGNATURE]',
          '[DISCLAIMER]',
        ],
      },
    ],
    prospect: [
      {
        body: `<p>[PROSPECT_FIRST_NAME],</p><p></p><p>I hope this email finds you well. I understand from my colleague that you've been introduced to [USER_COMPANY] and how we could possibly benefit your team at [PROSPECT_COMPANY].</p><p></p><p>I believe [USER_COMPANY] can make a significant difference [HOW_USER_SERVICE_CAN_HELP_PROSPECT_COMPANY].</p><p></p><p>[EXTENDED_DESCRIPTION_OF_USER_SERVICE].</p><p></p><p>I would love to discuss this further and answer any additional questions you may have about what [USER_COMPANY] can do for your organization.</p><p></p><p>I have a few slots left available this month, and I want to ensure [PROSPECT_COMPANY] gets the exclusive invite to be part of our [CURRENT_MONTH]-[NEXT_MONTH] group.</p><p></p><p>To schedule a meeting, pick whichever slot is convenient from the options below:</p><p>[TIMESLOT_#1]</p><p>[TIMESLOT_#2]</p><p>[TIMESLOT_#3]</p><p>None of the slots above work? View my full availability.</p><p></p><p>I'm looking forward to exploring how [USER_COMPANY] can support [PROSPECT_COMPANY].</p><p></p>[USER_SIGNATURE][DISCLAIMER]`,
        variables: [
          '[PROSPECT_FIRST_NAME]',
          '[USER_COMPANY]',
          '[PROSPECT_COMPANY]',
          '[HOW_USER_SERVICE_CAN_HELP_PROSPECT_COMPANY]',
          '[EXTENDED_DESCRIPTION_OF_USER_SERVICE]',
          '[CURRENT_MONTH]',
          '[NEXT_MONTH]',
          '[TIMESLOT_#1]',
          '[TIMESLOT_#2]',
          '[TIMESLOT_#3]',
          '[USER_SIGNATURE]',
          '[DISCLAIMER]',
        ],
      },
    ],
  },
  meetingBooked: {
    confirmed: {
      user: [
        {
          body: `<p>Hi [USER_FIRST_NAME] — We have an update to share with you.</p><p></p><p>[PROSPECT_FULL_NAME] has accepted your invitation (confirmed by the recipient). Their information is below:</p><p></p><p>Name: [PROSPECT_FULL_NAME]</p><p>Title: [PROSPECT_TITLE]</p><p>Company: [PROSPECT_COMPANY]</p><p>Location: [PROSPECT_LOCATION]</p><p>LinkedIn: [PROSPECT_LINKEDIN]</p><p>Contact: Email Address [PROSPECT_EMAIL]</p><p></p><p>Next Step: We'll send an introduction email to you both. Remember to initiate a conversation to minimize no-shows.</p><p></p><p>Here are some talking points, they booked the meeting from the presentation below:</p><p>[USER_KEY_SELLING_POINT_#1]</p><p>[USER_KEY_SELLING_POINT_#2]</p><p>[USER_KEY_SELLING_POINT_#3]</p><p></p><p>The meeting is confirmed and set for [MEETING_DATE_AND_TIME] [MEETING_TIMEZONE].</p><p></p><p>Meeting Questions:</p><p>How will this session help you? (It's OK if you don't know yet)</p><p>I will attend this session as I'm curious and look forward to learning more.</p><p></p><p>Enjoy the rest of your week!</p><p></p>[USER_SIGNATURE][DISCLAIMER]`,
          variables: [
            '[USER_FIRST_NAME]',
            '[PROSPECT_FULL_NAME]',
            '[PROSPECT_TITLE]',
            '[PROSPECT_COMPANY]',
            '[PROSPECT_LOCATION]',
            '[PROSPECT_LINKEDIN]',
            '[PROSPECT_EMAIL]',
            '[MEETING_DATE_AND_TIME]',
            '[MEETING_TIMEZONE]',
            '[USER_KEY_SELLING_POINT_#1]',
            '[USER_KEY_SELLING_POINT_#2]',
            '[USER_KEY_SELLING_POINT_#3]',
            '[USER_SIGNATURE]',
            '[DISCLAIMER]',
          ],
        },
      ],
      prospect: [
        {
          body: `<p>Hi [PROSPECT_FIRST_NAME],</p><p></p><p>Thank you so much for scheduling a meeting with [USER_COMPANY].</p><p></p><p>Below are a few details that we'll discuss during the meeting:</p><p>• [USER_VALUE_PROP_#1]</p><p>• [USER_VALUE_PROP_#2]</p><p>• [USER_VALUE_PROP_#3]</p><p></p><p>We've sent a calendar invite and included everything you’ll need to join us for the session.</p><p></p><p>Meeting joining info:</p><p>• Our meeting is scheduled for [MEETING_DATE_AND_TIME] [PROSPECT_TIMEZONE].</p><p>• You can join the meeting with the following: Meeting Conference Link.</p><p></p><p>If you change your mind about the schedule, you can reschedule for another convenient time.</p><p></p><p>Enjoy the rest of your week!</p><p></p>[USER_SIGNATURE][DISCLAIMER]`,
          variables: [
            '[PROSPECT_FIRST_NAME]',
            '[USER_COMPANY]',
            '[MEETING_DATE_AND_TIME]',
            '[PROSPECT_TIMEZONE]',
            '[USER_VALUE_PROP_#1]',
            '[USER_VALUE_PROP_#2]',
            '[USER_VALUE_PROP_#3]',
            '[USER_SIGNATURE]',
            '[DISCLAIMER]',
          ],
        },
      ],
    },
    '-1_hr': {
      user: [],
      prospect: [
        {
          body: `<p>Hey [PROSPECT_FIRST_NAME],</p><p></p><p>Thank you so much for scheduling a meeting with us!</p><p></p><p>Meeting joining info:</p><p> • Our meeting is scheduled for [MEETING_DATE_AND_TIME] [PROSPECT_TIMEZONE].</p><p> • You can join the meeting using the following: Meeting Conference Link.</p><p></p><p>Below are a few details on what we will discuss during the meeting.</p><p> • [USER_VALUE_PROP_#1]</p><p> • [USER_VALUE_PROP_#2]</p><p> • [USER_VALUE_PROP_#3]</p><p></p><p>If you change your mind about the schedule, you can reschedule for another convenient time.</p><p></p><p>Enjoy the rest of your week!</p><p></p>[USER_SIGNATURE][DISCLAIMER]`,
          variables: [
            '[PROSPECT_FIRST_NAME]',
            '[USER_COMPANY]',
            '[MEETING_DATE_AND_TIME]',
            '[PROSPECT_TIMEZONE]',
            '[USER_VALUE_PROP_#1]',
            '[USER_VALUE_PROP_#2]',
            '[USER_VALUE_PROP_#3]',
            '[USER_SIGNATURE]',
            '[DISCLAIMER]',
          ],
        },
      ],
    },
    '0_hr': {
      user: [],
      prospect: [
        {
          body: `<p>Hey [PROSPECT_FIRST_NAME],</p><p></p><p>Thank you so much for scheduling a meeting with us!</p><p></p><p>Meeting joining info:</p><p> • Our meeting is scheduled for [MEETING_DATE_AND_TIME] [PROSPECT_TIMEZONE].</p><p> • You can join the meeting using the following: Meeting Conference Link.</p><p></p><p>Below are a few details on what we will discuss during the meeting.</p><p> • [USER_VALUE_PROP_#1]</p><p> • [USER_VALUE_PROP_#2]</p><p> • [USER_VALUE_PROP_#3]</p><p></p><p>If you are unable to make this meeting, you may reschedule at your earliest convenience.</p><p></p><p>Enjoy the rest of your week!</p><p></p>[USER_SIGNATURE][DISCLAIMER]`,
          variables: [
            '[PROSPECT_FIRST_NAME]',
            '[USER_COMPANY]',
            '[MEETING_DATE_AND_TIME]',
            '[PROSPECT_TIMEZONE]',
            '[USER_VALUE_PROP_#1]',
            '[USER_VALUE_PROP_#2]',
            '[USER_VALUE_PROP_#3]',
            '[USER_SIGNATURE]',
            '[DISCLAIMER]',
          ],
        },
      ],
    },
  },
  missedMeeting: {
    user: [],
    prospect: [
      {
        body: `<p>Hi [PROSPECT_FIRST_NAME] — It looks like our scheduled meeting might have slipped through the cracks.</p><p></p><p>The meeting you requested was set for [MEETING_DATE_AND_TIME] [PROSPECT_TIMEZONE].</p><p></p><p>To refresh your memory, you've scheduled a meeting with [PROSPECT_COMPANY].</p><p></p><p>Could you reschedule at your earliest convenience?</p><p></p><p>Company: [USER_COMPANY]</p><p>Website: [USER_WEBSITE]</p><p></p><p>If you are no longer interested in receiving these, you may unenroll using the link in the email disclaimer at the bottom of this email.</p><p></p><p>Enjoy the rest of your week!</p><p></p>[USER_SIGNATURE][DISCLAIMER]`,
        variables: [
          '[PROSPECT_FIRST_NAME]',
          '[USER_COMPANY]',
          '[MEETING_DATE_AND_TIME]',
          '[PROSPECT_TIMEZONE]',
          '[PROSPECT_COMPANY]',
          '[USER_WEBSITE]',
          '[USER_SIGNATURE]',
          '[DISCLAIMER]',
        ],
      },
      {
        body: `<p>Hi [PROSPECT_FIRST_NAME] — It looks like our scheduled meeting might have slipped through the cracks.</p><p></p><p>The meeting you requested was set for [MEETING_DATE_AND_TIME] [PROSPECT_TIMEZONE].</p><p></p><p>To refresh your memory, you've scheduled a meeting with [PROSPECT_COMPANY].</p><p></p><p>Could you reschedule at your earliest convenience?</p><p></p><p>Company: [USER_COMPANY]</p><p>Website: [USER_WEBSITE]</p><p></p><p>If you are no longer interested in receiving these, you may unenroll using the link in the email disclaimer at the bottom of this email.</p><p></p><p>Enjoy the rest of your week!</p><p></p>[USER_SIGNATURE][DISCLAIMER]`,
        variables: [
          '[PROSPECT_FIRST_NAME]',
          '[USER_COMPANY]',
          '[MEETING_DATE_AND_TIME]',
          '[PROSPECT_TIMEZONE]',
          '[PROSPECT_COMPANY]',
          '[USER_WEBSITE]',
          '[USER_SIGNATURE]',
          '[DISCLAIMER]',
        ],
      },
      {
        body: `<p>Hi [PROSPECT_FIRST_NAME] — It looks like our scheduled meeting might have slipped through the cracks.</p><p></p><p>The meeting you requested was set for [MEETING_DATE_AND_TIME] [PROSPECT_TIMEZONE].</p><p></p><p>To refresh your memory, you've scheduled a meeting with [PROSPECT_COMPANY].</p><p></p><p>Could you reschedule at your earliest convenience?</p><p></p><p>Company: [USER_COMPANY]</p><p>Website: [USER_WEBSITE]</p><p></p><p>If you are no longer interested in receiving these, you may unenroll using the link in the email disclaimer at the bottom of this email.</p><p></p><p>Enjoy the rest of your week!</p><p></p>[USER_SIGNATURE][DISCLAIMER]`,
        variables: [
          '[PROSPECT_FIRST_NAME]',
          '[USER_COMPANY]',
          '[MEETING_DATE_AND_TIME]',
          '[PROSPECT_TIMEZONE]',
          '[PROSPECT_COMPANY]',
          '[USER_WEBSITE]',
          '[USER_SIGNATURE]',
          '[DISCLAIMER]',
        ],
      },
    ],
  },
};

module.exports = {
  TEMPLATE_VARS,
  EMAIL_JOURNEY_TIMELINES,
  TEMPLATES,
};
