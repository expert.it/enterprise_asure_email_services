const _24HrsInMS = 24 * 60 * 60 * 1000;
const getDaysInMS = (x) => x * 24 * 60 * 60 * 1000;
const getHrsInMS = (x) => x * 60 * 60 * 1000;
const getMinsInMS = (x) => x * 60 * 1000;
const getMsInMins = (x) => x / (1000 * 60);
const getDaysFromMins = (x) => x / (24 * 60);

const getCurrTimeObj = (args = {}) => {
  const { timeZone = 'GMT' } = args;
  const currTimeString = new Date().toLocaleTimeString('en-US', {
    timeZone,
    hour12: false,
  });
  const currTimeObj = {
    hrs: parseInt(currTimeString.split(':')[0]),
    mins: parseInt(currTimeString.split(':')[1]),
    secs: parseInt(currTimeString.split(':')[2]),
    timeZone,
  };
  return currTimeObj;
};

module.exports = {
  _24HrsInMS,
  getDaysInMS,
  getHrsInMS,
  getMinsInMS,
  getMsInMins,
  getDaysFromMins,
  getCurrTimeObj,
};
