const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const strip = (str, char) => {
  return str
    .replace(new RegExp(`^${char}+`), '')
    .replace(new RegExp(`${char}+$`), '');
};

const htmlToDOM = (htmlStr) => {
  const { document } = new JSDOM(htmlStr).window;
  return document;
};

const htmlToText = (htmlStr) => {
  const document = htmlToDOM(htmlStr);
  return document.body.textContent;
};

module.exports = {
  strip,
  htmlToText,
  htmlToDOM,
};
